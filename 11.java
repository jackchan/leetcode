class Solution {
    public int maxArea(int[] height) {
        int maxArea = 0 ;
        int maxLeftBoundaryIndex = 0;
        
        for (int i = 0; i <height.length -1 ; i++) {

            if (i!=0 && height[i] <= height[maxLeftBoundaryIndex])
                continue;

            for (int j = i + 1 ; j < height.length ; j++) {
                int area = Math.min(height[i], height[j]) * ( j - i);
                if (area > maxArea) {
                    maxArea = area;
                    maxLeftBoundaryIndex = i;
                }
            }
        }

        return maxArea;
    }
}