import java.util.Hashtable;

class Solution {
    public int romanToInt(String s) {
        Hashtable<String, Integer> h = new Hashtable<String, Integer>();

        h.put("I", 1);
        h.put("V", 5);
        h.put("X", 10);
        h.put("L", 50);
        h.put("C", 100);
        h.put("D", 500);
        h.put("M", 1000);
        h.put("IV", 4);
        h.put("IX", 9);
        h.put("XL", 40);
        h.put("XC", 90);
        h.put("CD",400);
        h.put("CM", 900);

        int result = 0;
        int i = 0;

        while (i < s.length()) {
            if (i+2 <= s.length()) {
                String subStr = s.substring(i, i+2);
                if (h.containsKey(subStr)) {
                    result += h.get(subStr);
                    i += 2;
                    continue;
                }
            }
            String subStr = s.substring(i, i+1);
            result += h.get(subStr);
            i++;
        }

        return result;
    }
}