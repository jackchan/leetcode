import java.util.*;

class Solution {

    public int minCost(int[][] grid) {
        Node[][] gridNode = new Node[grid.length][grid[0].length];
        List<Node> unvisited = new ArrayList<Node>(); 

        // populate a grid of nodes
        for (int i = 0 ; i < grid.length; i++)
            for (int j= 0; j<grid[0].length; j++) {
                gridNode[i][j] = new Node();
                gridNode[i][j].sign = grid[i][j];
                gridNode[i][j].x = i;
                gridNode[i][j].y = j;
                gridNode[i][j].prevNode = null;
                unvisited.add(gridNode[i][j]);
        
                if ((i != 0) || (j!=0)) {
                    gridNode[i][j].dist = Integer.MAX_VALUE;
                }
                else {
                    gridNode[i][j].dist = 0;
                }                    
            }
        
        while(!unvisited.isEmpty()){
            unvisited.sort( (Node n1, Node n2) -> n1.dist.compareTo(n2.dist));
            Node currentNode = unvisited.get(0);
            
            if (currentNode.x + 1 < grid.length) // down 
            {
                int newDist = (currentNode.sign == 3 ? currentNode.dist : currentNode.dist + 1);
                if (newDist < gridNode[currentNode.x + 1][currentNode.y].dist)
                    gridNode[currentNode.x +1][currentNode.y].dist = newDist;

           }
                
            if (currentNode.x - 1 >=0) //up
            {
                int newDist = (currentNode.sign == 4 ? currentNode.dist : currentNode.dist + 1);
                if (newDist < gridNode[currentNode.x - 1][currentNode.y].dist)
                    gridNode[currentNode.x - 1 ][currentNode.y].dist = newDist;
            }

            if (currentNode.y + 1 < grid[0].length) // right
            {
                int newDist = (currentNode.sign == 1 ? currentNode.dist : currentNode.dist + 1);
                if (newDist < gridNode[currentNode.x][currentNode.y + 1].dist)
                    gridNode[currentNode.x][currentNode.y + 1].dist = newDist;
            }

            if (currentNode.y - 1 >= 0) //left
            {
                int newDist = (currentNode.sign == 2 ? currentNode.dist : currentNode.dist + 1);
                if (newDist < gridNode[currentNode.x][currentNode.y - 1].dist)
                    gridNode[currentNode.x][currentNode.y - 1].dist = newDist;
            }

            unvisited.remove(0);

        }
            
        return gridNode[grid.length - 1][grid[0].length - 1].dist;
    }

    class Node {
        int x ;
        int y;
        Node prevNode;
        Integer dist;
        int sign;
    }
}