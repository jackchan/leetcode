import java.util.*;

class Solution {
    public void solveSudoku(char[][] board) {
        fillSudoku(board, 0);
    }

    public char[][] fillSudoku(char[][] board, int index) {

        while (index < 81 && board[index / 9][index % 9] != '.') {
            index++;
        }

        if (index == 81)
        return board;

        List<Integer> availableNumber = checkAvailableNumber(board, index/9 , index % 9 );    

        for (Integer num : availableNumber) {
            board[index / 9][index % 9] = num.toString().charAt(0);
            if (fillSudoku(board, index + 1) != null)
                return board;
            board[index / 9][index % 9] = '.';            
        }
      
        return null; // tried all avilableNumber
    }

    public List<Integer> checkAvailableNumber(char[][] board, int row , int col) {
        boolean[] availableNumber = new boolean[9];

        for (int i = 0 ; i < 9 ; i++)
            availableNumber[i] = true;

        for (int i = 0 ; i < 9 ; i++)
            if (board[row][i] != '.')
                availableNumber[Integer.parseInt(Character.toString(board[row][i])) - 1] = false;

        for (int i = 0 ; i < 9 ; i++)
            if (board[i][col] != '.')
                availableNumber[Integer.parseInt(Character.toString(board[i][col])) - 1] = false;

        int sectionX = row / 3;
        int sectionY = col / 3;

        for (int i = sectionX * 3 + 0 ; i < sectionX * 3 + 3 ; i++)
            for (int j = sectionY * 3 + 0 ; j < sectionY * 3 + 3; j++)
                if (board[i][j] != '.')
                    availableNumber[Integer.parseInt(Character.toString(board[i][j])) - 1] = false;
    
        List<Integer> numberList = new ArrayList<Integer>();
        for (int i = 0; i < 9; i++)
            if (availableNumber[i])
                numberList.add(i + 1);

        return numberList;
    }
}