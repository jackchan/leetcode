import java.util.*; 
import java.util.stream.*;

class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        List<Integer> list1 = IntStream.of(nums1).boxed().collect(Collectors.toList());
        List<Integer> list2 = IntStream.of(nums2).boxed().collect(Collectors.toList());
        
        ArrayList<Integer> newList = new ArrayList<Integer>();
        
        newList.addAll(list1);
        newList.addAll(list2);
        
        newList.sort(Comparator.naturalOrder());
        
        int size = list1.size() + list2.size();
        if ( size % 2 == 0 )
            return (newList.get(size / 2 - 1) + newList.get(size / 2)); 
        else 
            return (newList.get(size / 2));
    }
}