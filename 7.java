class Solution {
    public int reverse(int x) {
        Integer X = new Integer(x);
        boolean sign = false;

        if (X > 0) sign = true;
        
        X = Math.abs(X);
        String xStr = X.toString();

        //reverse the string
        char[] xChar = xStr.toCharArray();
        String reverseStr = "";
        for (int i = 0 ; i < xChar.length; i++)
            reverseStr += xChar[xChar.length - 1 - i];

        if (!sign)
            reverseStr = "-" + reverseStr;

        int ans = 0;
        try {
            ans = Integer.parseInt(reverseStr);
        } catch (NumberFormatException e)
        {
            return 0;
        }

        return ans;
    }
}